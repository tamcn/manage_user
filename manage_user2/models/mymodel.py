from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
)

from .meta import Base


class MyModel(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    value = Column(Integer)


Index('my_index', MyModel.name, unique=True, mysql_length=255)


class User(Base):
    __tablename__ = 'user'
    user_name = Column(Text, primary_key=True)
    pass_word = Column(Text, nullable=False)
    full_name = Column(Text, nullable=False)
    age = Column(Integer, nullable=False)
    sex = Column(Text, nullable=False)


Index('user_index', User.user_name, unique=True, mysql_length=255)