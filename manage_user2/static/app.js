var app = angular.module("myApp", []);

app.controller("myCtrl", function($scope, $http) {

    $scope.userDetail = null;

    $scope.getUserDetail = function (user) {
      var username = user.username;
      $http.get('http://localhost:6543/api/user/' + username)
          .then(function (response) {
              $scope.userDetail = response.data.user;
          },
          function (error) {
              console.log(error);
          });
    };

    // worked
    $scope.init = function () {
        $http.get('http://localhost:6543/api/user')
        .then(function (response) {
            $scope.users = response.data.users;
        })
    };

    // worked
    $scope.create = function () {
        var user = {
            "username": $scope.userDetail.username,
            "password": $scope.userDetail.password,
            "fullname": $scope.userDetail.fullname,
            "age": $scope.userDetail.age,
            "sex": $scope.userDetail.sex
        };
        $http.post('http://localhost:6543/api/user', user)
            .then(function (response) {
                $scope.init();
                $scope.isAddUser = false;
                $scope.userDetail = null;
                window.alert("Create user " + user.username + " successfully");
            })
    }


    $scope.updateUser = function () {
        var user = {
            "username": $scope.userDetail.username,
            "password": $scope.userDetail.password,
            "fullname": $scope.userDetail.fullname,
            "age": $scope.userDetail.age,
            "sex": $scope.userDetail.sex
        };

        $http.put('http://localhost:6543/api/user/' + user.username, user)
            .then(function (response) {
                // for (var i = 0; i < $scope.users.length; i++) {
                //     if ($scope.users[i].username === $scope.userDetail.username) {
                //         $scope.users[i] = $scope.userDetail;
                //         $scope.userDetail = null;
                //         window.alert("Update user " + user.username + " successfully");
                //         break;
                //     }
                // }
                $scope.userDetail = null;
                window.alert("Update user " + user.username + " successfully");
                $scope.init();
            },
                function (error) {
                    console.log(error);
            });
    };

    // worked
    $scope.deleteUser = function (index) {
        var username = $scope.users[index].username;

        $http.delete('http://localhost:6543/api/user/' + username)
            .then(function (response) {
                $scope.users.splice(index, 1);
                window.alert("Delete user " + username + " successfully");
            })
    };


});